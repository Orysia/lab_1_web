﻿#include <iostream>
#include <string>
#include <curl/curl.h>

using  namespace std;

int main() 
{
    CURL* curl;
    CURLcode res;

    string func;
    cout << "Enter your function: ";
    cin >> func;

    string uri_func;
    curl = curl_easy_init();
    if (curl) 
    {
        char* output = curl_easy_escape(curl, func.c_str(), func.length());
        if (output)
        {
            uri_func = output;
            curl_free(output);
        }
       curl_easy_cleanup(curl);
    }

    string url = "https://newton.vercel.app/api/v2/integrate/" + uri_func;
    curl = curl_easy_init();
    if (curl) 
    {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fwrite);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }
    return 0;
}